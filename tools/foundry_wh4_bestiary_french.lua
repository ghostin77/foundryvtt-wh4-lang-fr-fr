
package.path = package.path .. ";lua_scripts/libraries/luajson/?.lua"
local JSON = require"json"

local path_in  = "/home/sigmar/work/foundryvtt/WFRP-4th-Edition-FoundryVTT/packs/"
local traitsf = "/home/sigmar/work/foundryvtt/foundryvtt-wh4-lang-fr-fr/compendium/wfrp4e.traits.json"
local beastsf = "/home/sigmar/work/foundryvtt/foundryvtt-wh4-lang-fr-fr/compendium/wfrp4e.bestiary.json"
local f1 = io.open(path_in .. "bestiary.db")

local f2 = io.open(traitsf)
local strjson = f2:read("*a")
f2:close()
local traits = JSON.decode(strjson)

local f3 = io.open(beastsf)
local beastjson = f3:read("*a")
f3:close()
local beast_french = JSON.decode(beastjson)

local function trim1(s)
   return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local line = f1:read()
while line do 
  --print(line)
  local beast = JSON.decode( line) 
  -- Get the french beast translation
  local sel_beastfr
  for _, beastfr in pairs(beast_french.entries) do 
    if beast.name:lower() == beastfr.id:lower() then 
      sel_beastfr = beastfr
      break
    end
  end
  if not sel_beastfr then print(">>>>>>>>>>>>>>> NO BEAST !!!", beast.name) end
  
  --print(beast.name, beast.items)
  if beast.items then 
    for _, traitbeast in pairs( beast.items) do 
      --print(beast.name, traitbeast)
      if traitbeast.name then 
        local found, ntentacle = false
        local name, bonus_or_category = traitbeast.name:match("([%w%s]+)%s%(([%s%w]+)%)")
        if not name then ntentacle, name  = traitbeast.name:match("(%d)x%s*(Tentacles)") end
        if not name then name = traitbeast.name end
        for _, traitdata in pairs(traits.entries) do 
          if traitdata.id == trim1(name) then    
            found = true
            traitbeast.name = traitdata.name
            traitbeast.data.description.value = traitdata.description
            --print("    Found trait " .. trait.name, traitdata.name)
            break
          end
        end
        if not found then 
          print("    > NOT FOUND !!!", beast.name, name, traitbeast.name)
        end
      end
    end
    sel_beastfr.items = beast.items
  end
  line = f1:read()
end
f1:close()

local jsonout = JSON.encode( beast_french ) 
local fout = io.open("beasts.json", "w+")
fout:write( jsonout )
fout:close()